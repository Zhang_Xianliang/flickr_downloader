import random

import requests

from user_agent import user_agent

from flickrapi import FlickrAPI
import os
from tqdm import tqdm


def get_content(url):
    try:
        header = {'user-agent': random.choice(user_agent)}
        r = requests.get(url, headers=header)
        r.raise_for_status()
        # r.encoding = r.apparent_encoding
        return r.content
    except:
        print("Failed at url: " + url)
        exit(-1)


def get_urls(flickr, ids):
    video_urls = []

    for id in tqdm(ids):
        results = flickr.photos.getSizes(photo_id=id)
        results = results['sizes']['size']
        for result in results:
            if result['media'] == "video":
                if result['label'] != "Video Player":
                    video_urls.append(result['source'])
                    break
    return video_urls


def get_video_ids(flickr, tag, license=''):
    video_ids = []
    results = flickr.photos.search(tags=tag, media="videos", license=license)
    num = int(results['photos']['total'])
    for photo in results['photos']['photo']:
        video_ids.append(photo['id'])

    return video_ids, num


def download(root, url):
    path = url.split("/")[5] + ".mp4"
    content = get_content(url)

    with open(root + path, 'wb') as f:
        f.write(content)


def main():
    print("Create Folders...")
    root = "./videos/"
    if not os.path.exists(root):
        os.mkdir(root)
    key = "b7bcfcbb143e337fd747b0c907c838ee"
    secret = "0c39388591bfb2ac"
    flickr = FlickrAPI(key, secret, format='parsed-json')

    tag = "Mount Fuji"
    print("Collecting videos' id...")
    ids, num = get_video_ids(flickr, tag)
    print("Collecting videos' url...")
    video_urls = get_urls(flickr, ids)

    print("Download videos...")
    for url in tqdm(video_urls):
        download(root, url)


if __name__ == '__main__':
    main()
